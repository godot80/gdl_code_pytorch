"""
Pytorch complementary toolbox
"""
import torch as t
from torch import nn
from typing import List, Any, Callable, Tuple


def first_tensor(tpl: Tuple[t.Tensor, t.Tensor]) -> t.Tensor:
    return tpl[0]


def duplicate_tensor(tensr: t.Tensor) -> Tuple[t.Tensor, t.Tensor]:
    return tensr, tensr


def map_tensors_ho(
    fn: Callable[[t.Tensor], t.Tensor]
) -> Callable[[Tuple[t.Tensor, t.Tensor]], Tuple[t.Tensor, t.Tensor]]:
    def transfor_tensors(tpl: Tuple[t.Tensor, t.Tensor]) -> Tuple[t.Tensor, t.Tensor]:
        return fn(tpl[0]), fn(tpl[1])

    return transfor_tensors


def get_params_from_layer(layer: nn.Module) -> List[Any]:
    """
    get params from a layer of torch sequential model
    """
    return [i[1] for i in layer.__dict__.items() if not i[0].startswith("_")]


def get_params_from_model(encoder_decoder: nn.Sequential) -> List[List[Any]]:
    """
    get params from a torch sequential model
    """
    return [get_params_from_layer(e) for e in encoder_decoder]


def create_state_from_result(res: Any) -> dict:
    return {"result": res, "state": res}


def create_state_from_tuple_ho(nb_res: int, name_res: List[str]):
    """
    create a state from a tuple of Tensors
    """

    def create_state_from_tuple(res: Tuple[t.Tensor, ...]) -> dict:
        dic = {}
        for i in range(nb_res):
            dic[name_res[i]] = res[i]
        return {"result": res, "state": dic}

    return create_state_from_tuple


def leak_norm_drop(
    model: nn.Sequential, dim_out: int, use_dropout: bool, use_batch_norm: bool
) -> nn.Sequential:
    """
    add the usual sequence to a neural network layer
    """
    model = nn.Sequential(*model, nn.LeakyReLU())
    if use_batch_norm:
        model = nn.Sequential(*model, nn.BatchNorm2d(num_features=dim_out))
    if use_dropout:
        model = nn.Sequential(*model, nn.Dropout(p=0.25))
    return model


def sampling(t_tpl: Tuple[t.Tensor, t.Tensor]) -> t.Tensor:
    mu, log_var = t_tpl
    if mu.shape != log_var.shape:
        raise ValueError("Passed array is not of the right shape")

    epsilon = t.normal(mu, t.exp(log_var) / 2)  # type: ignore
    return epsilon  # {"state": {"mu": mu, "log_var": log_var, "result": epsilon}}
