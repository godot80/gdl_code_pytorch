import torch as t
from models.toolbox.pytorch_modules import (
    Y_pipeline,
    Y_pipeline_merge,
    Lambda,
    Reshape,
    Partial_reshape,
)


def test_lambda():
    tnsr = t.Tensor(1)
    assert Lambda(lambda tnsr: tnsr)(tnsr) == tnsr
    tnsr2 = t.Tensor(1, 2, 3)
    assert all(
        Lambda(lambda tnsr: tnsr)(tnsr2).isclose(tnsr2, equal_nan=True).reshape(-1)
    )


def test_reshape():
    tnsr = t.Tensor(1)
    reshape = Reshape(1)
    print("shape:", reshape(tnsr).shape)
    assert reshape(tnsr).shape == tnsr.shape
    tnsr = t.Tensor(1, 2)
    reshape = Reshape(-1)
    assert reshape(tnsr).shape == t.Size([2])


def test_partial_reshape():
    tnsr = t.Tensor(2, 2, 3)
    partial_reshape = Partial_reshape(6)
    assert partial_reshape(tnsr).shape == (2, 6)


def test_y_pipeline():
    assert type(Y_pipeline_merge(lambda tpl: tpl[0])) == Y_pipeline_merge


def test_y_pipeline_merge():
    assert type(Y_pipeline(lambda tnsr: (tnsr, tnsr))) == Y_pipeline
