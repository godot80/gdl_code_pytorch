import torch as t
from torch import nn
import pytest
from models.toolbox.pytorch_toolbox import (
    first_tensor,
    duplicate_tensor,
    map_tensors_ho,
    sampling,
)
from models.toolbox.pytorch_modules import Y_pipeline, Y_pipeline_merge


def test_map_tensors():
    x = t.Tensor(2, 2)
    y = t.Tensor(2, 2)
    assert map_tensors_ho(lambda tnsr: tnsr)((x, y)) == (x, y)

    assert all(
        [
            all(tnsr[0] == tnsr[1])
            for tnsr in list(
                zip(
                    map_tensors_ho(lambda tnsr: t.flatten(tnsr))((x, y)),  # type: ignore
                    (t.flatten(x), t.flatten(y)),  # type: ignore
                )
            )
        ]
    )


def test_duplicate_tensor():
    dedup_dup_test_helper((1))
    dedup_dup_test_helper((2, 1))
    dedup_dup_test_helper((4, 4, 7))


def dedup_dup_test_helper(shape):
    y_pipe_first = Y_pipeline_merge(first_tensor)
    x = t.Tensor(shape)
    y = t.Tensor(shape)
    tpl = (x, y)
    assert type(y_pipe_first(tpl)) == t.Tensor
    assert y_pipe_first(tpl).shape == x.shape

    y_pipe_first(tpl).isclose(tpl[0], equal_nan=True)

    tnsr_dup_dedup = nn.Sequential(
        Y_pipeline(duplicate_tensor), Y_pipeline_merge(first_tensor)
    )
    assert all(tnsr_dup_dedup(x).isclose(x, equal_nan=True).reshape(-1))
    tnsr_dedup_dup = nn.Sequential(
        Y_pipeline_merge(first_tensor), Y_pipeline(duplicate_tensor)
    )
    tpl_res = tnsr_dedup_dup((x, y))
    assert all([all(tnsr.isclose(x, equal_nan=True).reshape(-1)) for tnsr in tpl_res])


def test_sampling():
    x = t.Tensor(1, 1)
    y = t.Tensor(1, 1)
    tpl = (x, y)
    assert type(sampling(tpl)) == t.Tensor
    assert sampling(tpl).shape == x.shape

    x = t.Tensor(1)
    y = t.Tensor(2)
    tpl = (x, y)
    with pytest.raises(ValueError) as err_info:
        sampling(tpl)
        assert "not of the right shape" in err_info
    mu_one = t.ones(1)  # type: ignore
    log_no_var = t.tensor([-100000.0])

    assert sampling((mu_one, log_no_var)) == mu_one

    mus = t.tensor([1.0, 2.0, 3.0])
    log_no_vars = t.tensor(
        [
            -100000.0,
            -100000.0,
            -100000.0,
        ]
    )

    assert all(sampling((mus, log_no_vars)) == mus)

    mu_matrix = t.tensor([[1.0, 2.0], [3.0, 4.0]])
    log_no_vars_matrix = t.tensor([[-100000.0, -100000.0], [-10000.0, -10000.0]])

    assert t.all(sampling((mu_matrix, log_no_vars_matrix)) == mu_matrix)  # type: ignore
