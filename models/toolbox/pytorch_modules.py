"""
Pytorch complementary Module Class
"""
import torch as t
from torch import nn
from typing import Callable, Tuple, Any, TypedDict


# From Fast.ai
LambdaFunc = Callable[[Any], Any]


class State_m(TypedDict):
    result: t.Tensor
    state: dict


class Lambda(nn.Module):
    "An easy way to create a pytorch layer for a simple `func`."

    def __init__(self, func: LambdaFunc):
        "create a layer that simply calls `func` with `x`"
        super().__init__()
        self.func = func

    def forward(self, x: Any):
        return self.func(x)


class Y_pipeline(nn.Module):
    def __init__(self, func: Callable[[t.Tensor], Tuple[t.Tensor, t.Tensor]]):
        super().__init__()
        self.function = func

    def forward(self, tensor: t.Tensor):
        return self.function(tensor)


class Y_pipeline_merge(nn.Module):
    def __init__(self, func: Callable[[Tuple[t.Tensor, t.Tensor]], t.Tensor]):
        super().__init__()
        self.function = func

    def forward(self, tpl: Tuple[t.Tensor, t.Tensor]):
        return self.function(tpl)


class Reshape(nn.Module):
    """
    Pytorch version of TF reshape function
    """

    def __init__(self, *args):
        super(Reshape, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(*self.shape)


class Partial_reshape(nn.Module):
    """
    Partial reshape function
    """

    def __init__(self, *args):
        super(Partial_reshape, self).__init__()
        self.partial_shape = args

    def forward(self, x):
        return x.view(x.shape[0], *self.partial_shape)


class Sequential_state(nn.Sequential):
    r"""A sequential container with state."""

    def __init__(self, *args: Any):
        super(Sequential_state, self).__init__(*args)
        self.state = {}

    def forward(self, input: Any) -> State_m:
        for module in self:
            output = module(input)
            if type(output) == dict and "state" in output.keys():
                input = output["result"]
                new_state = output["state"]
                self.state = {**self.state, **new_state}
            else:
                input = output
        return {"result": input, "state": self.state}
