"""
Build an autoencoder recursively
"""
import torch.nn as nn

from .toolbox.pytorch_modules import Partial_reshape
from typing import List, NamedTuple, Union


def leak_norm_drop(model, dim_out, use_dropout, use_batch_norm):
    model = nn.Sequential(*model, nn.LeakyReLU())
    if use_batch_norm:
        model = nn.Sequential(*model, nn.BatchNorm2d(num_features=dim_out))
    if use_dropout:
        model = nn.Sequential(*model, nn.Dropout(p=0.25))
    return model


# struct like immutable class
class Encoder_layer(NamedTuple):
    """
    encoder layer of an encoder-decoder model
    """

    filter: int
    kernel_size: int
    strides: int
    pads: int
    output_pads: int = -1


class Decoder_layer(NamedTuple):
    """
    decoder layer of an encoder-decoder model
    """

    filter: int
    kernel_size: int
    strides: int
    pads: int
    output_pads: int


# new type
enc_dec_lay = Union[List[Encoder_layer], List[Decoder_layer]]


def recurs_build_model_layers(
    model_type: str,
    n_channel_in: int,
    size_out: int,
    coder_layers: enc_dec_lay,
    use_batch_norm: bool,
    use_dropout: bool,
) -> nn.Sequential:
    """
    Recursive immutable encoder & decoder model builder

    model_type: encoder or decoder
    n_channel_in: number of channel
    size_out: ?
    coder_layers: parameters of each layer of the model
    use_batch_norm: use batch normalization at each layer
    use_dropout: use drop out at each layer
    """
    # compute needed vars
    # n_layer = len(coder_layers) + 2
    conv_filters = [c.filter for c in coder_layers]
    # assert conv_filters
    if model_type == "encoder":
        dims_in = [n_channel_in, *conv_filters[:-1]]
        dims_out = [*conv_filters]
    elif model_type == "decoder":
        dims_in = [*conv_filters]
        dims_out = [*conv_filters[1:], n_channel_in]
    else:
        raise Exception("Unknown model type " + str(model_type))

    def build_layers(
        model: nn.Sequential,
        coder_layers: enc_dec_lay,
        dims_in: List[int],
        dims_out: List[int],
    ):
        # layer_nb = n_layer - len(coder_layers)
        layer = coder_layers.pop(0)
        dim_in = dims_in.pop(0)
        dim_out = dims_out.pop(0)

        if model_type == "encoder":
            conv_layer = nn.Conv2d(
                dim_in,  # input dim
                dim_out,  # output dim
                kernel_size=layer.kernel_size,
                stride=layer.strides,
                padding=layer.pads,
            )
            # always add a convolution & relu layer
            model = nn.Sequential(*model, conv_layer)
        elif model_type == "decoder":
            conv_t_layer = nn.ConvTranspose2d(
                dim_in,
                dim_out,
                kernel_size=layer.kernel_size,
                stride=layer.strides,
                padding=layer.pads,
                output_padding=layer.output_pads,
            )
            model = nn.Sequential(*model, conv_t_layer)
        else:
            raise Exception("Unknown model type " + str(model_type))

        if model_type == "decoder" and len(coder_layers) == 0:
            final = nn.Sequential(*model, nn.Sigmoid())
        else:
            final = leak_norm_drop(model, dim_out, use_dropout, use_batch_norm)

        if len(coder_layers) == 0:
            return final
        return build_layers(final, coder_layers, dims_in, dims_out)

    model_pre = (
        nn.Sequential(
            nn.Linear(2, conv_filters[0] * size_out ** 2),
            Partial_reshape(conv_filters[0], size_out, size_out),
        )
        if model_type == "decoder"
        else nn.Sequential()
    )
    model_post = (
        nn.Sequential(nn.Flatten(1), nn.Linear(conv_filters[-1] * size_out ** 2, 2))
        if model_type == "encoder"
        else nn.Sequential()
    )

    model_build = build_layers(model_pre, coder_layers, dims_in, dims_out)
    return nn.Sequential(*model_build, *model_post)


def build_encoder_decoder(
    n_channel_input: int,
    encoder_layers: List[Encoder_layer],
    decoder_layers: List[Decoder_layer],
    size_out: int,
    use_batch_norm: bool = False,
    use_dropout: bool = False,
) -> nn.Sequential:
    """
    build encoder and decoder
    """

    encoder = recurs_build_model_layers(
        "encoder",
        n_channel_input,
        size_out,
        encoder_layers,
        use_batch_norm,
        use_dropout,
    )
    decoder = recurs_build_model_layers(
        "decoder",
        n_channel_input,
        size_out,
        decoder_layers,
        use_batch_norm,
        use_dropout,
    )
    encoder_decoder = nn.Sequential(*(list(encoder) + list(decoder)))
    return encoder_decoder
