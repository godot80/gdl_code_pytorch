import pytest
import torch.nn as nn
from models.autoencoder import (
    Encoder_layer,
    Decoder_layer,
    recurs_build_model_layers,
    build_encoder_decoder,
)


def test_recurs_build_model_layers():
    layer = Encoder_layer(filter=32, kernel_size=3, strides=1, pads=1)
    encoder = [layer]
    with pytest.raises(Exception) as gen_ex:
        recurs_build_model_layers("not_ae", 1, 2, encoder, False, False)
    assert "Exception" in str(gen_ex)
    assert "Unknown model" in str(gen_ex)
    # layer = Encoder_layer(filter=32, kernel_size=3, strides=1, pads=1)
    # encoder = [layer]
    model = recurs_build_model_layers("decoder", 1, 2, encoder, False, False)
    assert type(model) == nn.Sequential


def test_build_encoder_decoder():
    e_layer = Encoder_layer(filter=32, kernel_size=3, strides=1, pads=1)
    encoder = [e_layer]
    d_layer = Decoder_layer(filter=32, kernel_size=3, strides=1, pads=1, output_pads=1)
    decoder = [d_layer]
    build_encoder_decoder(1, encoder, decoder, 1, False, False)
    pass


def test_Encoder_layer():
    pass


def test_Decoder_layer():
    pass
