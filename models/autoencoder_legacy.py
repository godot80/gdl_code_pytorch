"""
LEGACY Build an autoencoder recursively
"""
import torch.nn as nn

# from torch.nn.modules.container import Sequential
from .toolbox.pytorch_toolbox import Partial_reshape
from typing import List


def recurs_build_model_layers(
    model_type,
    n_channel_in,
    size_out,
    conv_filters,
    conv_kernel_size,
    conv_strides,
    conv_pads,
    conv_output_pads,
):
    """
    Recursive immutable encoder & decoder model builder
    """
    # compute needed vars
    n_layer = len(conv_filters) + 2
    layer_dims = (
        [n_channel_in] + conv_filters
        if model_type == "encoder"
        else conv_filters + [n_channel_in]
    )
    # print('enc_layer:', layer_dims)
    # nc_img_div = np.prod(conv_strides)

    def build_layers(
        model,
        layer_conv_filters,
        layer_conv_kernel_sizes,
        layer_conv_strides,
        layer_conv_pads,
        layer_conv_out_pads,
    ):
        layer_nb = str(n_layer - len(layer_conv_filters))

        if model_type == "encoder":
            conv_layer = nn.Conv2d(
                layer_conv_filters.pop(0),
                layer_conv_filters[0],
                kernel_size=layer_conv_kernel_sizes.pop(0),
                stride=layer_conv_strides.pop(0),
                padding=layer_conv_pads.pop(0),
            )
            model.add_module("conv_" + layer_nb, conv_layer)
            model.add_module("relu_" + layer_nb, nn.LeakyReLU())
        elif model_type == "decoder":
            conv_layer = nn.ConvTranspose2d(
                layer_conv_filters.pop(0),
                layer_conv_filters[0],
                kernel_size=layer_conv_kernel_sizes.pop(0),
                stride=layer_conv_strides.pop(0),
                padding=layer_conv_pads.pop(0),
                output_padding=layer_conv_out_pads.pop(0),
            )
            model.add_module("conv_" + layer_nb, conv_layer)
            if len(layer_conv_kernel_sizes) == 0:
                model.add_module("activation", nn.Sigmoid())
            else:
                model.add_module("relu_" + layer_nb, nn.LeakyReLU())
        else:
            raise Exception("Unknown model type " + str(model_type))

        # print("encoder_conv_filters", encoder_conv_filters)
        if len(layer_conv_kernel_sizes) > 0:
            return build_layers(
                model,
                layer_conv_filters,
                layer_conv_kernel_sizes,
                layer_conv_strides,
                layer_conv_pads,
                layer_conv_out_pads,
            )
        return model

    # create model
    model = nn.Sequential()

    if model_type == "decoder":
        model.add_module("dense", nn.Linear(2, conv_filters[0] * size_out ** 2))
        model.add_module(
            "reshape", Partial_reshape(conv_filters[0], size_out, size_out)
        )

    model = build_layers(
        model,
        layer_dims.copy(),
        conv_kernel_size.copy(),
        conv_strides.copy(),
        conv_pads.copy(),
        conv_output_pads.copy() if conv_output_pads else None,
    )

    if model_type == "encoder":
        # todo find immutable add_module
        model.add_module("flatten", nn.Flatten())
        model.add_module("dense", nn.Linear(conv_filters[-1] * size_out ** 2, 2))

    return model


def build_encoder_decoder_legacy(
    n_channel_input: int,
    encoder_conv_filters: List,
    encoder_conv_kernel_size: List,
    encoder_conv_strides: List,
    encoder_conv_pads: List,
    decoder_conv_t_filters: List,
    decoder_conv_t_kernel_size: List,
    decoder_conv_t_strides: List,
    decoder_conv_t_pads: List,
    decoder_conv_t_output_pads: List,
) -> nn.Sequential:
    """
    build encoder and decoder
    """

    encoder = recurs_build_model_layers(
        "encoder",
        n_channel_input,
        7,
        encoder_conv_filters,
        encoder_conv_kernel_size,
        encoder_conv_strides,
        encoder_conv_pads,
        None,
    )
    decoder = recurs_build_model_layers(
        "decoder",
        n_channel_input,
        7,
        decoder_conv_t_filters,
        decoder_conv_t_kernel_size,
        decoder_conv_t_strides,
        decoder_conv_t_pads,
        decoder_conv_t_output_pads,
    )
    encoder_decoder = nn.Sequential(*(list(encoder) + list(decoder)))
    return encoder_decoder
