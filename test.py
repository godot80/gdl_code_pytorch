from typing import List
from models.toolbox.pytorch_toolbox import get_params_from_model
from models.autoencoder import build_encoder_decoder, Decoder_layer, Encoder_layer
from models.autoencoder_legacy import build_encoder_decoder_legacy

n_channel_input = 1
input_dim = (28, 28, 1)
n_channel_input

lay_1 = Encoder_layer(filter=32, kernel_size=3, strides=1, pads=1)
lay_2 = Encoder_layer(filter=64, kernel_size=3, strides=2, pads=1)
lay_3 = Encoder_layer(filter=64, kernel_size=3, strides=2, pads=1)
lay_4 = Encoder_layer(filter=64, kernel_size=3, strides=1, pads=1)

encoder_layers: List[Encoder_layer] = [lay_1, lay_2, lay_3, lay_4]

d_lay_1 = Decoder_layer(filter=64, kernel_size=3, strides=1, pads=1, output_pads=0)
d_lay_2 = Decoder_layer(filter=64, kernel_size=3, strides=2, pads=1, output_pads=1)
d_lay_3 = Decoder_layer(filter=32, kernel_size=3, strides=2, pads=1, output_pads=1)
d_lay_4 = Decoder_layer(filter=1, kernel_size=3, strides=1, pads=1, output_pads=0)

decoder_layers: List[Decoder_layer] = [d_lay_1, d_lay_2, d_lay_3, d_lay_4]

encoder_decoder = build_encoder_decoder(n_channel_input, encoder_layers, decoder_layers)

encoder_conv_filters = [lay_1.filter, lay_2.filter, lay_3.filter, lay_4.filter]
encoder_conv_kernel_size = [3, 3, 3, 3]
encoder_conv_strides = [1, 2, 2, 1]
encoder_conv_pads = [
    1,
    1,
    1,
    1,
]  # padding = 'same'

decoder_conv_t_filters = [64, 64, 32, 1]
decoder_conv_t_kernel_size = [3, 3, 3, 3]
decoder_conv_t_strides = [1, 2, 2, 1]
decoder_conv_t_pads = [1, 1, 1, 1]
decoder_conv_t_output_pads = [0, 1, 1, 0]

encoder_conv_1 = [32, 3, 1, 1]

encoder_decoder_leg = build_encoder_decoder_legacy(
    n_channel_input,
    encoder_conv_filters,
    encoder_conv_kernel_size,
    encoder_conv_strides,
    encoder_conv_pads,
    decoder_conv_t_filters,
    decoder_conv_t_kernel_size,
    decoder_conv_t_strides,
    decoder_conv_t_pads,
    decoder_conv_t_output_pads,
)

# print(encoder_decoder)
# print(encoder_decoder2)
assert get_params_from_model(encoder_decoder) == get_params_from_model(
    encoder_decoder_leg
)
